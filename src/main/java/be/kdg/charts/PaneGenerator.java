package be.kdg.charts;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static be.kdg.charts.ChartApplication.*;

public class PaneGenerator {
    private final Group root;
    private String language;
    private int analyseIndex;
    private ChartType chartType;

    public PaneGenerator(String language, int analyseIndex, ChartType chartType) {
        this.language = language;
        this.analyseIndex = analyseIndex;
        this.chartType = chartType;
        root = new Group();
    }

    private void updateMap() throws IOException {
        Map map = mapper.readValue(new File(basepath + language + (analyseIndex + 1) +".json"), HashMap.class);
        root.getChildren().removeAll(root.getChildren());
        switch (chartType) {
            case PIE: root.getChildren().add(ChartGenerator.pieChart(analyses[analyseIndex], map)); break;
            case BAR: root.getChildren().add(ChartGenerator.barchart(analyses[analyseIndex], map, false)); break;
            case BAR_PERCENTAGE: root.getChildren().add(ChartGenerator.barchart(analyses[analyseIndex], map, true)); break;
            case LINE: root.getChildren().add(ChartGenerator.lineChart(analyses[analyseIndex], map, false)); break;
            case LINE_PERCENTAGE: root.getChildren().add(ChartGenerator.lineChart(analyses[analyseIndex], map, true)); break;
        }

    }

    public BorderPane generatePane() throws IOException {
        updateMap();

        BorderPane pane = new BorderPane();

        VBox vboxLanguages = new VBox();
        vboxLanguages.setPadding(new Insets(15, 12, 15, 12));
        vboxLanguages.setSpacing(10);
        vboxLanguages.setStyle("-fx-background-color: #336699;");
        pane.setLeft(vboxLanguages);

        for (String language : languages) {
            Button button = generateVBoxLanguageButton(language);
            vboxLanguages.getChildren().add(button);
        }

        VBox vboxChartTypes = new VBox();
        vboxChartTypes.setPadding(new Insets(15, 12, 15, 12));
        vboxChartTypes.setSpacing(10);
        vboxChartTypes.setStyle("-fx-background-color: #336699;");
        pane.setRight(vboxChartTypes);

        for (ChartType chartType : ChartType.values()) {
            Button button = generateVBoxChartTypeButton(chartType.getDescription());
            vboxChartTypes.getChildren().add(button);
        }

        FlowPane hbox = new FlowPane();
        hbox.setPadding(new Insets(15, 12, 15, 12));
        hbox.setStyle("-fx-background-color: #336699;");
        hbox.setAlignment(Pos.TOP_CENTER);
        pane.setBottom(hbox);

        for (String analyse : analyses) {
            Button button = generateHBoxButton(analyse);
            hbox.getChildren().add(button);
        }

        pane.setCenter(root);
        return pane;
    }

    private Button generateVBoxChartTypeButton(String label) {
        Button newButton = new Button(label);
        newButton.setPrefSize(100, 20);
        newButton.setOnAction((EventHandler) event -> {
            Button source = (Button)event.getSource();
            String buttonText = source.getText();
            System.out.println("You clicked a chartType:" + buttonText);
            chartType = ChartType.fromDesciption(buttonText);
            try {
                updateMap();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return newButton;
    }

    private Button generateVBoxLanguageButton(String label) {
        Button newButton = new Button(label);
        newButton.setPrefSize(100, 20);
        newButton.setOnAction((EventHandler) event -> {
            Button source = (Button)event.getSource();
            String buttonText = source.getText();
            System.out.println("You clicked a language:" + buttonText);
            language = buttonText;
            try {
                updateMap();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return newButton;
    }

    private Button generateHBoxButton(String label) {
        Button newButton = new Button(label);
        newButton.setPrefSize(500, 20);
        newButton.setOnAction((EventHandler) event -> {
            Button source = (Button)event.getSource();
            String buttonText = source.getText();
            analyseIndex = Arrays.asList(analyses).indexOf(buttonText);
            System.out.println("You clicked an analysis:" + buttonText);
            try {
                updateMap();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return newButton;
    }

}
