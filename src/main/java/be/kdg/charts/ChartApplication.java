package be.kdg.charts;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class ChartApplication extends Application {

    static String basepath = "D:\\analyses\\";
    static String[] languages = new String[]{"Deens","Duits","Engels","Fins","Italiaans","Nederlands","Portugees","Spaans"};
    static String[] analyses = new String[]{
            "Aantal woorden dat begint met",
            "Aantal woorden dat eindigt met",
            "Aantal dat voorkomt in corpus",
            "Verdeling tussen klinkers en medeklinkers",
            "Top 25 bigrams waarmee een woord begint",
            "Top 25 bigrams waarmee een woord eindigt",
            "Top 25 bigrams in corpus",
            "Top 25 trigrams in corpus",
            "Top 25 skipgrams in corpus",
            "Aantal voorkomen bigrams die overeenkomen met de top 25 skipgrams in corpus",
    };
    static ObjectMapper mapper = new ObjectMapper();

    @Override
    public void start(Stage stage) throws IOException {
        PaneGenerator generator = new PaneGenerator("Nederlands", 5, ChartType.PIE);
        Scene scene = new Scene(generator.generatePane(), 1920, 1080);
        stage.setTitle("CL analyse");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}