package be.kdg.charts;

public enum ChartType {
    BAR("bar"),
    BAR_PERCENTAGE("bar %"),
    PIE("pie"),
    LINE("line"),
    LINE_PERCENTAGE("line %");

    public String getDescription() {
        return description;
    }

    final String description;

    ChartType(String description) {
        this.description = description;
    }

    public static ChartType fromDesciption(String description) {
        for (ChartType b : ChartType.values()) {
            if (b.description.equalsIgnoreCase(description)) {
                return b;
            }
        }
        return null;
    }
}
