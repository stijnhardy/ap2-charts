package be.kdg.charts;

import javafx.collections.FXCollections;
import javafx.scene.chart.*;

import java.util.*;
import java.util.stream.Collectors;

public class ChartGenerator {

    public static LineChart lineChart(String title, Map<Object, Object> rawdata, boolean percentage) {
        Map<String, Number> data =  rawdata.entrySet().stream().collect(Collectors.toMap(entry -> (String)entry.getKey(), entry -> Integer.parseInt((String) entry.getValue())));
        int total = calculateTotal(data);
        LineChart linechart = new LineChart(getXAxis(data.keySet()), getYAxis());
        linechart.setTitle(title);

        for(Map.Entry<String, Number> datapoint : data.entrySet()) {
            XYChart.Series series = new XYChart.Series();
            series.setName(title);
            series.setName(datapoint.getKey());
            if(percentage)
                series.getData().add(new XYChart.Data(datapoint.getKey(), (int)datapoint.getValue() * 100 / total));
            else
                series.getData().add(new XYChart.Data(datapoint.getKey(), datapoint.getValue()));
            linechart.getData().add(series);
        }
        linechart.setMinWidth(1400);

        return linechart;
    }

    public static PieChart pieChart(String title, Map<Object, Object> rawdata) {
        Map<String, Number> data =  rawdata.entrySet().stream().collect(Collectors.toMap(entry -> (String)entry.getKey(), entry -> Integer.parseInt((String) entry.getValue())));

        List<PieChart.Data> pieChartData = new ArrayList<>();

        for(Map.Entry<String, Number> datapoint : data.entrySet()) {
            PieChart.Data newData = new PieChart.Data(datapoint.getKey(), datapoint.getValue().doubleValue());
            pieChartData.add(newData);
        }

        PieChart pieChart = new PieChart(FXCollections.observableArrayList(pieChartData));
        pieChart.setTitle(title);
        pieChart.setLabelsVisible(true);
        pieChart.setMinWidth(1400);
        return pieChart;
    }

    public static Chart barchart(String title, Map<Object, Object> rawdata, boolean percentage) {

        Map<String, Number> data =  rawdata.entrySet().stream().collect(Collectors.toMap(entry -> (String)entry.getKey(), entry -> Integer.parseInt((String) entry.getValue())));
        int total = calculateTotal(data);
        BarChart<String, Number> barChart = new BarChart<>(getXAxis(data.keySet()), getYAxis());
        barChart.setTitle(title);

        //Add data to the chart
        for(Map.Entry<String, Number> datapoint : data.entrySet()) {
            XYChart.Series<String, Number> series = new XYChart.Series<>();
            series.setName(datapoint.getKey());
            if(percentage)
                series.getData().add(new XYChart.Data<>(datapoint.getKey(), (int)datapoint.getValue() * 100 / (int)total));
            else
                series.getData().add(new XYChart.Data<>(datapoint.getKey(), datapoint.getValue()));
            barChart.getData().add(series);
        }

        barChart.setMinWidth(1400);

        return barChart;
    }

    private static int calculateTotal(Map<String, Number> data) {
        Number total = 0;
        Optional calculatedTotal = data.values().stream().reduce((x, y) -> (int)x + (int)y);
        if(calculatedTotal.isPresent())
            total = (Number)calculatedTotal.get();
        return (int)total;
    }

    private static CategoryAxis getXAxis(Set<String> keys) {
        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("Text");
        xAxis.setCategories(FXCollections.<String>observableArrayList(keys));
        return xAxis;
    }

    private static NumberAxis getYAxis() {
        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Amount");
        return yAxis;
    }
}
