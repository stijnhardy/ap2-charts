module be.kdg.charts {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.fasterxml.jackson.databind;

    opens be.kdg.charts to javafx.fxml;
    exports be.kdg.charts;
}